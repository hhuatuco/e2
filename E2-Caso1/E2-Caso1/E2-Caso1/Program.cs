﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace E2_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lista de programas:\n");
            Stack<string> programas = new Stack<string>();

            //Agregando programas
            programas.Push("Programa 1:" + "Suma");
            programas.Push("Programa 2:" + "Resta");
            programas.Push("Programa 3:" + "Multiplicación");
            programas.Push("Programa 4:" + "División");
            programas.Push("Programa 5:" + "Potenciación");

            foreach (var elemento in programas)
            {
                Console.WriteLine(elemento);
            }
            Console.WriteLine("");

            //Procesando los 2 ultimos en llegar
            Console.WriteLine("Procesando el primer programa...\n");
            programas.Pop();

            foreach (var elemento in programas)
            {
                Console.WriteLine(elemento);
            }
            Console.WriteLine("");

            Console.WriteLine("Procesando el segundo programa...\n");
            programas.Pop();

            Console.WriteLine("Mostrando la lista después de procesar los 2 últimos programas:\n");
            foreach (var elemento in programas)
            {
                Console.WriteLine(elemento);
            }
            Console.WriteLine("");

            Console.Read();
        }
    }
}
