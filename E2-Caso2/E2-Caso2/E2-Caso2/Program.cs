﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E2_Caso2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lista de 5 alumnos con prioridad normal:\n");
            Queue<string> alumno = new Queue<string>();

            //Ingresando 5 alumnos con prioridad normal
            alumno.Enqueue("ID: N0001"+" "+"Nombre y Apellido: Hugo Huatuco");
            alumno.Enqueue("ID: N0002"+" "+"Nombre y Apellido: Alejandro Lopez");
            alumno.Enqueue("ID: N0003"+" "+"Nombre y Apellido: Arturo Castillo");
            alumno.Enqueue("ID: N0004"+" "+"Nombre y Apellido: Luis Alfaro");
            alumno.Enqueue("ID: N0005"+ " "+"Nombre y Apellido: Eduardo Poma");

            foreach (var elemento in alumno)
            {
                Console.WriteLine(elemento);
            }
            Console.WriteLine(" ");

            Console.WriteLine("Ingresando Alumno con prioridad:\n");

            ColaDePrioridad.Nodo cola1;
            cola1 = ColaDePrioridad.NuevoNodo(1,"N0001","Hugo","Huatuco");
            cola1 = ColaDePrioridad.push(cola1,4, "N0002", "Alejandro","Lopez");
            cola1 = ColaDePrioridad.push(cola1,5, "N0003", "Arturo", "Castillo");
            cola1 = ColaDePrioridad.push(cola1,2, "N0004","Luis", "Alfaro");
            cola1 = ColaDePrioridad.push(cola1, 3, "N0005", "Eduardo", "Poma");
            cola1 = ColaDePrioridad.push(cola1, 0, "N0009", "Enrique", "Carmona");

            while (!ColaDePrioridad.isEmpty(cola1))
            {
                Console.WriteLine("ID:  {0:D}", ColaDePrioridad.top(cola1));
                cola1 = ColaDePrioridad.pop(cola1);
            }
            Console.Read();
         }
    }
    class ColaDePrioridad
        {
            public class Nodo
            {
                public int prioridad;
                public string ID;
                public string Nombre;
                public string Apellido;
                public Nodo siguiente;
            }

        public static Nodo nodo = new Nodo();
            public static Nodo NuevoNodo(int prioridad, string ID, string Nombre, string Apellido )
            {
                Nodo temporal = new Nodo();
                temporal.prioridad = prioridad;
                temporal.ID = ID;
                temporal.Nombre = Nombre;
                temporal.Apellido = Apellido;
                temporal.siguiente = null;
                return temporal;
            }

        public static string top(Nodo cabeza)
        {
                return (cabeza).ID +" "+"Nombre y Apellido: "+ (cabeza).Nombre +" "+(cabeza).Apellido;
                
        }

            public static Nodo pop(Nodo cabeza)
            {
                Nodo temp = cabeza;
                (cabeza) = (cabeza).siguiente;
                return cabeza;
            }

            public static Nodo push(Nodo cabeza, int prioridad, string ID, string Nombre, string Apellido)
            {
                Nodo puntero = (cabeza);
                Nodo temp = NuevoNodo(prioridad, ID,Nombre,Apellido);
                if ((cabeza).prioridad > prioridad)
                {
                    temp.siguiente = cabeza;
                    (cabeza) = temp;
                }
                else
                {
                    while (puntero.siguiente != null &&
                        puntero.siguiente.prioridad < prioridad)
                    {
                        puntero = puntero.siguiente;
                    }
                    temp.siguiente = puntero.siguiente;
                    puntero.siguiente = temp;
                }
                return cabeza;
            }

            public static bool isEmpty(Nodo cabeza)
            {
                if (cabeza == null)
                {
                    return true;
                }
                return false;
            }

        }
    }
    

